var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var app = express();

// установить предпочитаемый шаблон
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

// Ошибка 404
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// Рендарить шаблон error в случае ошибки 500
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// Рендарить шаблон error в случае ошибки 500
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
// прослушивание 3000 порта

app.listen(3000);

module.exports = app;